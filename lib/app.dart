import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'routes/routes.dart';
import 'theme/theme.dart';

class GitHubApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent),
    );

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: GitHubThemeData.themeData,
      darkTheme: GitHubThemeData.themeData,
      getPages: AppPages.pages,
//      locale: LocalizationService.locale,
//      fallbackLocale: LocalizationService.fallbackLocale,
//      translations: LocalizationService(),

      defaultTransition: Transition.native,
      initialRoute: Routes.SPLASH,
    );
  }
}
