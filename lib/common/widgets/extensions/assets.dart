extension ImageAssetExt on String {
  String get pathPNG => 'assets/images/$this.png';
}
