import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/models/video_model.dart';
import 'package:github_app/routes/routes.dart';
import 'package:github_app/services/firebase_service.dart';

class HomeController extends GetxController {
  final _service = FireBaseService();

  final videoModels = <VideoModel>[].obs;

  final listVideoLike = <VideoModel>[].obs;

  int _prevVideo = 0;

  final pageController = PageController(
    initialPage: 0,
    viewportFraction: 1,
  );

  @override
  void onInit() {
    super.onInit();
    _fetchVideos();
    loadVideo(0);
  }

  void _fetchVideos() async {
    final _result = await _service.getVideoList();
    videoModels.value = _result;
  }

  void onTapListLike() async {
    videoModels[_prevVideo].controller!.pause();
    await Get.toNamed(Routes.LIST_LIKE);
    videoModels[_prevVideo].controller!.play();
  }

  void onTapAddVideo() async {
    videoModels[_prevVideo].controller!.pause();
    await Get.toNamed(Routes.ADD_VIDEO);
    videoModels[_prevVideo].controller!.play();
  }

  void changeVideo(index) async {
    if (videoModels[index].controller == null) {
      await videoModels[index].loadController();
    }
    videoModels[index].controller!.play();
    if (videoModels[_prevVideo].controller != null) videoModels[_prevVideo].controller!.pause();
    _prevVideo = index;
  }

  void loadVideo(int index) async {
    if (videoModels.length > index) {
      await videoModels[index].loadController();
      videoModels[index].controller?.play();
    }
  }
}
