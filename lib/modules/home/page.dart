import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/common/common.dart';
import 'package:github_app/modules/card_video/page.dart';

import 'controller.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _buildContent(),
          const Icon(Icons.add).elevatedButton(onPressed: controller.onTapAddVideo),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Expanded(
      child: Obx(() {
        final models = controller.videoModels.value;
        return Stack(
          children: [
            Container(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: PageView.builder(
                controller: controller.pageController,
                itemCount: models.length,
                onPageChanged: (index) {
                  index = index % (models.length);
                  controller.changeVideo(index);
                },
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  index = index % (models.length);
                  return CardVideoPage(
                    model: models[index],
                  );
                },
              ),
            ),
            InkWell(
              onTap: controller.onTapListLike,
              child: Image.asset(
                'ic_list_like'.pathPNG,
                width: 26,
                color: Colors.white,
              ).paddingOnly(left: 20, top: 45),
            ),
          ],
        );
      }),
    );
  }
}
