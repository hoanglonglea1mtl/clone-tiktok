export 'splash/controller.dart';
export 'splash/page.dart';
export 'home/controller.dart';
export 'home/page.dart';
export 'list_like/page.dart';
export 'list_like/controller.dart';
export 'add_video/page.dart';
export 'add_video/controller.dart';
