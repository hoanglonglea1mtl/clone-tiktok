import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/modules/list_like/widget/item_video_like.dart';

import '../modules.dart';
import 'controller.dart';

class ListLikePage extends GetView<ListLikeController> {
  final _homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video đã thích'),
      ),
      body: Obx(
        () => GridView.count(
          crossAxisCount: 3,
          childAspectRatio: 0.7,
          padding: const EdgeInsets.only(top: 10),
          mainAxisSpacing: 2,
          crossAxisSpacing: 2,
          children: _homeController.listVideoLike.value.map((item) {
            return ItemVideoLike(
              model: item,
              onTap: () => controller.onTapItemVideoLike(item),
            );
          }).toList(),
        ),
      ),
    );
  }
}
