import 'package:get/get.dart';
import 'package:github_app/models/models.dart';
import 'package:github_app/modules/card_video/page.dart';

class ListLikeController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  void onTapItemVideoLike(VideoModel model) async {
    model.controller!.play();
    await Get.to(CardVideoPage(model: model, isBack: true));
    model.controller!.pause();
  }
}
