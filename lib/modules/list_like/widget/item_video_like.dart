import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/models/models.dart';
import 'package:github_app/modules/card_video/page.dart';
import 'package:video_player/video_player.dart';
import 'package:github_app/common/common.dart';

class ItemVideoLike extends StatelessWidget {
  final VideoModel model;
  final VoidCallback? onTap;

  ItemVideoLike({required this.model,this.onTap});
  final _theme = Get.theme;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        alignment: Alignment.bottomLeft,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: SizedBox(
              width: model.controller?.value.size.width ?? 0,
              height: model.controller?.value.size.height ?? 0,
              child: VideoPlayer(model.controller!),
            ),
          ),
          Row(
            children: [
              const Icon(
                Icons.play_arrow_outlined,
                size: 20,
              ),
              4.horizontalSpace,
              Text(
                model.likes,
                style: _theme.textTheme.caption,
              )
            ],
          ),
        ],
      ),
    );
  }
}
