import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/common/common.dart';
import 'package:video_player/video_player.dart';

import 'controller.dart';

class AddVideoPage extends GetView<AddVideoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Thêm video'),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(
          () => controller.video.value.path.isEmpty ? Text('Chọn video') : _buildShowVideo(),
        ),
        20.verticalSpace,
        _buildButton(),
      ],
    ).paddingSymmetric(vertical: 12);
  }

  Widget _buildShowVideo() {
    return Expanded(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: VideoPlayer(controller.videoController!),
          ),
          Center(
            child: InkWell(
              onTap: controller.uploadVideo,
              child: Icon(
                Icons.file_upload,
                size: 100,
                color: Colors.white.withOpacity(0.6),
              ),
            ),
          ),
          if (controller.loading.value) _buildLoading(),
        ],
      ),
    );
  }

  Widget _buildButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.camera_alt).outlinedButton(onPressed: controller.pickVideoCamera).wrapWidth(100),
        20.horizontalSpace,
        Icon(Icons.folder).outlinedButton(onPressed: controller.pickVideoGallery).wrapWidth(100),
      ],
    );
  }

  Widget _buildLoading() {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      color: Colors.black.withOpacity(0.8),
      child: CircularProgressIndicator(),
    );
  }
}
