import 'dart:io';
import 'package:github_app/models/models.dart';
import 'package:github_app/modules/home/controller.dart';
import 'package:github_app/services/firebase_service.dart';
import 'package:path/path.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

class AddVideoController extends GetxController {
  final _homeController = Get.find<HomeController>();

  final _service = FireBaseService();

  final loading = false.obs;

  final Rx<XFile> video = XFile('').obs;

  final ImagePicker _picker = ImagePicker();

  VideoPlayerController? videoController;

  void pickVideoGallery() async {
    final XFile? _video = await _picker.pickVideo(source: ImageSource.gallery);
    if (_video != null) {
      video.value = _video;
      videoController = VideoPlayerController.file(File(_video.path));
      _loadVideoController(_video);
    }
  }

  void pickVideoCamera() async {
    final XFile? _video = await _picker.pickVideo(source: ImageSource.camera);
    if (_video != null) {
      video.value = _video;
      _loadVideoController(_video);
    }
  }

  Future<void> _loadVideoController(XFile file) async {
    videoController = VideoPlayerController.file(File(file.path));
    await videoController?.initialize();
    videoController?.setLooping(true);
    if (videoController != null) {
      videoController!.play();
    }
  }

  void uploadVideo() async {
    loading.value = true;
    try {
      final _file = video.value;
      final _fileName = basename(_file.path);
      final _destination = 'files/$_fileName';
      final _task = _service.uploadFile(_destination, File(_file.path));
      if (_task == null) return;
      final _snapshot = await _task.whenComplete(() {});

      final _urlDownload = await _snapshot.ref.getDownloadURL();
      _addVideoInList(url: _urlDownload);
      Get.back();
      loading.value = false;
      Get.snackbar('Thành công', 'Tải video lên thành công');
    } catch (e) {
      loading.value = false;
      Get.snackbar('Lỗi', e.toString());
    }
  }

  void _addVideoInList({required String url}) {
    final _newModel = VideoModel(
        id: DateTime.now().microsecondsSinceEpoch.toString(),
        user: 'New user',
        userPic: 'https://www.geo.tv/assets/uploads/updates/2021-02-22/336312_4237556_updates.jpg',
        videoTitle: 'New video',
        songName: 'Song play',
        likes: '212',
        comments: '23',
        url: url);
    _homeController.videoModels.value.add(_newModel);
    _homeController.videoModels.refresh();
  }

  @override
  void dispose() {
    videoController!.dispose();
    super.dispose();
  }
}
