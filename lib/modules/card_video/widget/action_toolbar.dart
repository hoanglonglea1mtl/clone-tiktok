import 'package:flutter/material.dart';
import 'package:github_app/common/common.dart';
import 'package:get/get.dart';
import 'package:github_app/models/video_model.dart';
import '../controller.dart';

class ActionsToolbar extends StatelessWidget {
  final VideoModel model;

  ActionsToolbar({required this.model});
  late CardVideoController _controller;
  @override
  Widget build(BuildContext context) {
    _controller = Get.find<CardVideoController>(tag: model.id);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _getFollowAction(pictureUrl: model.userPic),
        20.verticalSpace,
        Obx(
          () => _getSocialAction(
            icon: 'ic_heart',
            title: model.likes,
            isHeartFocus: _controller.isLike.value,
            onTap: () => _controller.actionLike(model: model),
          ),
        ),
        _getSocialAction(icon: 'ic_comment', title: model.comments),
        _getSocialAction(icon: 'ic_share', title: 'Share'),
        50.verticalSpace,
        _getMusicPlayerAction(userPic: model.userPic),
        10.verticalSpace,
      ],
    ).paddingOnly(right: 5);
  }

  Widget _getSocialAction({required String title, required String icon, VoidCallback? onTap, bool isHeartFocus = false}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: onTap,
          child: Image.asset(
            icon.pathPNG,
            width: 30,
            color: isHeartFocus ? Colors.redAccent : Colors.white,
          ),
        ),
        5.verticalSpace,
        Text(title),
        15.verticalSpace,
      ],
    );
  }

  Widget _getFollowAction({@required pictureUrl}) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          height: 50,
          width: 50,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Colors.white)),
          child: CachedImageNetworkWidget(url: pictureUrl),
        ),
        Container(
          width: 20,
          height: 20,
          decoration: const BoxDecoration(color: Colors.red, shape: BoxShape.circle),
          child: const Icon(
            Icons.add,
            color: Colors.white,
            size: 20.0,
          ),
        )
      ],
    );
  }

  LinearGradient get musicGradient => LinearGradient(
      colors: [Colors.grey[800]!, Colors.grey[900]!, Colors.grey[900]!, Colors.grey[800]!], stops: [0.0, 0.4, 0.6, 1.0], begin: Alignment.bottomLeft, end: Alignment.topRight);

  Widget _getMusicPlayerAction({@required userPic}) {
    return CircleImageAnimation(
      child: Container(
        width: 60,
        height: 60,
        padding: const EdgeInsets.all(15),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        decoration: BoxDecoration(shape: BoxShape.circle, gradient: musicGradient),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(1000),
          child: CachedImageNetworkWidget(url: userPic),
        ),
      ),
    );
  }
}
