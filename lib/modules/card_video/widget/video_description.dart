import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/common/common.dart';
import 'package:marquee/marquee.dart';

class VideoDescription extends StatelessWidget {
  final username;
  final videoTitle;
  final songInfo;

  VideoDescription({required this.username, this.videoTitle, @required this.songInfo});
  final _theme = Get.theme;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '@ $username',
          style: _theme.textTheme.subtitle2!.medium.textWhite,
        ),
        5.verticalSpace,
        Text(
          videoTitle ?? '',
          style: _theme.textTheme.subtitle2!.regular.textWhite,
        ),
        5.verticalSpace,
        _buildMusic(),
      ],
    ).paddingSymmetric(vertical: 12, horizontal: 16);
  }

  Widget _buildMusic() {
    return Row(children: [
      Image.asset(
        'ic_note_music'.pathPNG,
        color: Colors.white,
        width: 14,
      ),
      10.horizontalSpace,
      SizedBox(
        width: 200,
        height: 20,
        child: Marquee(
          text: songInfo ?? "---",
          style: _theme.textTheme.subtitle2!.regular.textWhite,
          scrollAxis: Axis.horizontal,
          crossAxisAlignment: CrossAxisAlignment.start,
          blankSpace: 50,
          velocity: 50,
          pauseAfterRound: 1.seconds,
          startPadding: 0.0,
          accelerationDuration: 1.seconds,
          accelerationCurve: Curves.linear,
          decelerationDuration: 1.seconds,
          decelerationCurve: Curves.bounceIn,
        ),
      ),
    ]);
  }
}
