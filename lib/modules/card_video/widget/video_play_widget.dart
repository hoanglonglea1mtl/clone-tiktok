import 'package:flutter/material.dart';
import 'package:github_app/models/video_model.dart';
import 'package:video_player/video_player.dart';
import 'package:get/get.dart';

class VideoPlayWidget extends StatefulWidget {
  final VideoModel model;

  VideoPlayWidget({required this.model});

  @override
  _VideoPlayWidgetState createState() => _VideoPlayWidgetState();
}

class _VideoPlayWidgetState extends State<VideoPlayWidget> {
  final isPlay = true.obs;

  final showVideo = false.obs;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.model.controller != null) {
      widget.model.controller!.addListener(() {
        showVideo.value = widget.model.controller != null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onTapPlayPause,
      child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: double.infinity,
          child: Obx(
            () => showVideo.value
                ? Stack(
                    alignment: Alignment.center,
                    children: [
                      SizedBox(
                        width: widget.model.controller?.value.size.width ?? 0,
                        height: widget.model.controller?.value.size.height ?? 0,
                        child: VideoPlayer(widget.model.controller!),
                      ),
                      Obx(
                        () => AnimatedOpacity(
                          opacity: isPlay.value ? 0 : 1,
                          duration: 150.milliseconds,
                          child: Icon(
                            Icons.play_arrow,
                            size: 80,
                            color: Colors.white.withOpacity(0.8),
                          ),
                        ),
                      ),
                    ],
                  )
                : CircularProgressIndicator(),
          )),
    );
  }

  void _onTapPlayPause() {
    if (isPlay.value) {
      widget.model.controller!.pause();
    } else {
      widget.model.controller!.play();
    }
    isPlay.value = !isPlay.value;
  }
}
