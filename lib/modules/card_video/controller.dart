import 'package:get/get.dart';
import 'package:github_app/models/video_model.dart';
import 'package:github_app/modules/home/controller.dart';

class CardVideoController extends GetxController {
  final homeController = Get.find<HomeController>();

  final isLike = false.obs;

  final showHeart = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  void actionLike({required VideoModel model}) {
    isLike.value = !isLike.value;
    final _isHave = homeController.listVideoLike.contains(model);
    if (isLike.value) {
      if (!_isHave) homeController.listVideoLike.value.add(model);
    } else {
      if (_isHave) homeController.listVideoLike.value.removeWhere((element) => element.id == model.id);
    }
    homeController.listVideoLike.refresh();
  }

  void onDoubleTapInVideo({required VideoModel model}) {
    isLike.value = true;
    showHeart.value = true;
    if (!homeController.listVideoLike.contains(model)) {
      homeController.listVideoLike.value.add(model);
      homeController.listVideoLike.refresh();
    }
    Future.delayed(500.milliseconds, () {
      showHeart.value = false;
    });
  }
}
