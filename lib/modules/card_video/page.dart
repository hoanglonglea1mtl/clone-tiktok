import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/models/video_model.dart';
import 'package:video_player/video_player.dart';

import '../modules.dart';
import 'controller.dart';
import 'widget/widget.dart';
import 'package:github_app/common/common.dart';

class CardVideoPage extends StatelessWidget {
  final VideoModel model;
  final bool isBack;

  CardVideoPage({required this.model, this.isBack = false});

  final _theme = Get.theme;

  late CardVideoController _controller;

  @override
  Widget build(BuildContext context) {
    _controller = Get.put(CardVideoController(), tag: model.id);
    return Scaffold(
      body: InkWell(
        onDoubleTap: () => _controller.onDoubleTapInVideo(model: model),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            VideoPlayWidget(model: model),
            ActionsToolbar(model: model),
            if (isBack) Positioned(top: 30, left: 10, child: const BackButton()),
            _buildHeart(),
            VideoDescription(
              songInfo: model.songName,
              username: model.user,
              videoTitle: model.videoTitle,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildHeart() {
    return Obx(
      () {
        return Align(
          alignment: Alignment.center,
          child: AnimatedOpacity(
            duration: 150.milliseconds,
            opacity: _controller.showHeart.value ? 1 : 0,
            child: AnimatedContainer(
              duration: 150.milliseconds,
              width: _controller.showHeart.value ? 110 : 100,
              child: Image.asset(
                'ic_heart'.pathPNG,
                width: double.infinity,
                color: Colors.redAccent,
              ),
            ),
          ),
        );
      },
    );
  }
}
