import 'package:get/get.dart';
import 'package:github_app/routes/routes.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    Future.delayed(2.seconds, () => _navigatorNextPage());
  }

  void _navigatorNextPage() {
    Get.offAndToNamed(Routes.HOME);
  }
}
