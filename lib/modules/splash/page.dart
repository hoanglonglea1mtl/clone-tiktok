import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_app/common/common.dart';

import 'controller.dart';

class SplashPage extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('SPLASH SCREEN')),
    );
  }
}
