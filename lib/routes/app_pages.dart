import 'package:get/get.dart';
import 'package:github_app/modules/modules.dart';
import 'app_routes.dart';

abstract class AppPages {
  static final List<GetPage<dynamic>> pages = [
    GetPage(
      name: Routes.SPLASH,
      page: () => SplashPage(),
      bindings: [
        BindingsBuilder.put(() => SplashController()),
      ],
    ),
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
      bindings: [
        BindingsBuilder.put(() => HomeController()),
      ],
    ),
    GetPage(
      name: Routes.LIST_LIKE,
      page: () => ListLikePage(),
      bindings: [
        BindingsBuilder.put(() => ListLikeController()),
      ],
    ),
    GetPage(
      name: Routes.ADD_VIDEO,
      page: () => AddVideoPage(),
      bindings: [
        BindingsBuilder.put(() => AddVideoController()),
      ],
    ),
  ];
}
