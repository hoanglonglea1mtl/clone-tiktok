abstract class Routes {
  static const SPLASH = '/';
  static const HOME = '/home';
  static const LIST_LIKE = '/list_like';
  static const ADD_VIDEO = '/add_video';
}
